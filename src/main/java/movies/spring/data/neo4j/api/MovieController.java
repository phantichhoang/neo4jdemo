package movies.spring.data.neo4j.api;

import movies.spring.data.neo4j.movies.DetailsDto;
import movies.spring.data.neo4j.movies.ResultDto;
import movies.spring.data.neo4j.movies.Service;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
class MovieController {

	private final Service movieService;

	MovieController(Service movieService) {
		this.movieService = movieService;
	}

	/**
	 * Detail movie
	 * @param title
	 * @return
	 */
	@GetMapping("/movie/{title}")
	public DetailsDto findByTitle(@PathVariable("title") String title) {
		return movieService.fetchDetailsByTitle(title);
	}

	/**
	 * Vote movie
	 * @param title
	 * @return
	 */
	@PostMapping("/movie/{title}/vote")
	public int voteByTitle(@PathVariable("title") String title) {
		return movieService.voteInMovieByTitle(title);
	}

	/**
	 * Search movie
	 * @param title
	 * @return
	 */
	@GetMapping("/movie/search")
	List<ResultDto> search(@RequestParam("q") String title) {
		return movieService.searchMoviesByTitle(stripWildcards(title));
	}

	@GetMapping("/graph")
	public Map<String, List<Object>> getGraph() {
		return movieService.fetchMovieGraph();
	}

	private static String stripWildcards(String title) {
		String result = title;
		if (result.startsWith("*")) {
			result = result.substring(1);
		}
		if (result.endsWith("*")) {
			result = result.substring(0, result.length() - 1);
		}
		return result;
	}
}
