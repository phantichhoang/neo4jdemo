package movies.spring.data.neo4j.person;

import org.springframework.data.neo4j.core.schema.Id;
import org.springframework.data.neo4j.core.schema.Node;

@Node
public class Person {

	@Id
	private final String name;

	private Integer yearBorn;

	public Person(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public Integer getYearBorn() {
		return yearBorn;
	}

	public void setYearBorn(Integer yearBorn) {
		this.yearBorn = yearBorn;
	}

}
