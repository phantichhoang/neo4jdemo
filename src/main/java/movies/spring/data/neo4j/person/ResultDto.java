package movies.spring.data.neo4j.person;

import movies.spring.data.neo4j.movies.Movie;

public record ResultDto(Movie movie) {
}
