package movies.spring.data.neo4j.movies;

import java.util.List;

public record DetailsDto(String title, List<CastMemberDto> cast) {
}
