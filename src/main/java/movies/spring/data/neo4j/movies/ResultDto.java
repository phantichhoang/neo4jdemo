package movies.spring.data.neo4j.movies;

public record ResultDto(Movie movie) {
}
